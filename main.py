import flask
from flask import request
import pandas_datareader as dr
import pymysql
import requests

app = flask.Flask(__name__)
host_name = "localhost"
port = 3306
user_name = "todolegal"
user_password = "todolegal"
db_name = "exchange_db"
db_table = 'eurusd'
webhook_url = 'https://webhook.site/3e5f6b0e-b3b3-4637-9278-341666e0e082'

@app.route('/', methods=['GET'])
def home():
    return "<h1>Test API todo legal</h1>"

@app.route('/get_information/', methods=['POST'])
def get_information():
    '''
    Web services with POST METHOD
    json structure
        {
        "date": "2022-09-01"
        }
    -if error exist return:
        {
        'error': 'No hay datos para la consulta solicitada'
        }
    -if NO error exist return:
        {
        'date_consultation': DATE,
        'value_exchange': VALUE
        }
    '''
    if request.method == 'POST':
        dic = request.json
        inf_db = search_in_db(dic['date'])
        result = {'error': 'No hay datos para la consulta solicitada'}
        if inf_db != 'error':
            if inf_db:
                pass
            else:
                con_yahoo = bring_save_from_yahooo(dic['date'])
                if con_yahoo:
                    inf_db = search_in_db(dic['date'])
            result = {'date_consultation': dic['date'], 'value_exchange': inf_db[0][2]}
            # Hit to webhook
            response = requests.post(webhook_url, json=result)
            if response.status_code == 200:
                pass
        return result


def search_in_db(date):
    '''
    Search in database rows from given date
    return False if rows don't exist
    return rows if rows don't exist
    '''
    result = False
    try:
        db = pymysql.connect(host=host_name, port=port, user=user_name, passwd=user_password, database=db_name)
        cursor = db.cursor()
        sql = f"SELECT * FROM {db_table} WHERE date = '{date}'"
        cursor.execute(sql)
        results = cursor.fetchall()
        if results:
            result = results
        else:
            result = False
    except:
        result = 'error'
    return result

def bring_save_from_yahooo(date):
    '''
        Bring information from Yahoo searching by date given
        return False if no value is in Yahoo
        return True if a new row is saved in database
        '''
    try:
        eurusd = dr.data.DataReader('EURUSD%3DX', data_source='yahoo', start=date, end=date)
    except:
        return False
    db = pymysql.connect(host=host_name, port=port, user=user_name, passwd=user_password, database=db_name)
    cursor = db.cursor()
    sql = f"INSERT INTO {db_table}(date, value) VALUES ('{date}','{eurusd['Adj Close'].values[0]}')"
    try:
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()
    return True

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1556)
app.run()

