# Todolegal Quiz
# By Lenin Vargas
# Instalación y configuración base de datos mysql
sudo apt update

sudo apt update

sudo apt install mysql-server -Y

mysql --version

sudo mysql

# Creación usuario de base de datos y provilegios

CREATE USER 'todolegal'@'localhost' IDENTIFIED BY 'todolegal';

GRANT ALL PRIVILEGES ON * . * TO 'todolegal'@'localhost';

FLUSH PRIVILEGES;

# Creción de base de datos y tablas

CREATE DATABASE exchange_db;

USE exchange_db;

CREATE TABLE eurusd (id INT(11) NOT NULL AUTO_INCREMENT, date DATE, value FLOAT, PRIMARY KEY (id));

show tables;

describe eurusd;
